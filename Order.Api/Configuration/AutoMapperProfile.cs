﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Order.Domain;
using Order.Domain.Events;
using Order.Dto;

namespace Order.Api.Configuration
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<OrderDto, Domain.Order>()
                .ForMember(x => x.ID, opt => opt.Ignore())
                .ForMember(x => x.Status, opt => opt.Ignore())
                .ForMember(x => x.ModifyDate, opt => opt.Ignore())
                .ForMember(x => x.CreateDate, opt => opt.Ignore());

            CreateMap<CreateOrderDto, Domain.Order>()
                .ForMember(x => x.Status, x => x.MapFrom(src => true))
                .ForMember(x => x.CreateDate, x => x.MapFrom(src => DateTime.Now))
                .ForMember(x => x.ModifyDate, x => x.MapFrom(src => DateTime.Now));

            CreateMap<Domain.Order, OrderDto>()
                .ForMember(x => x.OrderId, x => x.MapFrom(src => src.ID));
            CreateMap<CommunicationInfoDto, CommunicationInfo>()
                .ForMember(x => x.IsNotified, opt => opt.Ignore());
            CreateMap<CommunicationInfoLogDto, CommunicationInfoLog>();
            CreateMap<CommunicationInfo, CommunicationInfoDto>();
        }
    }
}
