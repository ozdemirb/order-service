﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Order.Dto;
using Order.Service;

namespace Order.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommunicationInfoController : ControllerBase
    {
        private readonly ICommunicationInfoService _communicationInfoService;

        public CommunicationInfoController(ICommunicationInfoService communicationInfoService)
        {
            _communicationInfoService = communicationInfoService;
        }
    }
}
