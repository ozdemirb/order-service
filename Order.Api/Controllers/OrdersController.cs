﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Order.Dto;
using Order.Service;

namespace Order.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly ICommunicationInfoService _communicationInfoService;

        public OrdersController(IOrderService orderService, ICommunicationInfoService communicationInfoService)
        {
            _orderService = orderService;
            _communicationInfoService = communicationInfoService;
        }

        [HttpGet("{orderId}")]
        public ActionResult<OrderDto> Get(Guid orderId)
        {
            var response = _orderService.GetOrder(orderId);
            return Ok(response);
        }

        [HttpPost]
        public ActionResult CreateOrder(CreateOrderDto request)
        {
            var response=_orderService.CreateOrder(request);
            return Created("", response);
        }

        [HttpPatch("{orderId}/cancel")]
        public ActionResult UpdateOrderStatus(Guid orderId,OrderStatusOnlyDto request)
        {
            request.OrderId = orderId;
           var response= _orderService.UpdateOrderStatus(orderId, request);
           return Ok(response);
        }

        [HttpGet("{orderId}/communicationInfo")]
        public ActionResult GetOrderCommunicationInfo(Guid orderId)
        {
            var response=_communicationInfoService.GetCommunicationInfo(orderId);
            return Ok(response);
        }
        
        [HttpGet("{orderId}/communicationInfoLog")]
        public ActionResult GetOrderCommunicationInfoLog(Guid orderId)
        {
            var response=_communicationInfoService.GetOrderCommunicationInfoLog(orderId);
            return Ok(response);
        }
        
    }
}
