﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Order.Crosscutting.Enums;
using Order.Crosscutting.Exceptions;

namespace Order.Api.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        public readonly RequestDelegate next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await this.next(httpContext);
            }
            catch (Exception ex)
            {
                await this.HandleExceptionAsync(httpContext, ex);
            }
        }

        public Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            ProblemDetails problemDetails = new ProblemDetails();
            context.Response.ContentType = "application/json";

            if (ex is BaseException)
            {
                var exception = (BaseException)ex;
                var status = (int)GetStatus(exception.ErrorType);
                context.Response.StatusCode = status;
                problemDetails = new ProblemDetails()
                {
                    Type = exception.Type,
                    Detail = exception.Detail,
                    Status = status,
                    Extensions = { ["params"] = exception.EntityName, ["message"] = $"error.{exception.ErrorKey}" }
                };
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError; ;
                problemDetails = new ProblemDetails()
                {
                    Type = "internal-server-exception",
                    Detail = ex.Message,
                    Status = StatusCodes.Status500InternalServerError
                };
            }
            return HttpResponseWritingExtensions.WriteAsync(context.Response, JsonConvert.SerializeObject(problemDetails), new CancellationToken());
        }

        private HttpStatusCode GetStatus(ErrorType type)
        {
            switch (type)
            {
                case ErrorType.NotFound: return HttpStatusCode.NotFound;
                case ErrorType.BadRequest: return HttpStatusCode.BadRequest;
            }

            return HttpStatusCode.BadRequest;
        }
    }
}
