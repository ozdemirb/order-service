using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Order.Api.Configuration;
using Order.Api.Middleware;
using Order.BackGroundServices;
using Order.Data;
using Order.Domain;
using Order.Queue;
using Order.Service;
using Order.Service.Validation;
using Order.Validation;
using OrderEvent = Order.Domain.OrderEvent;

namespace Order.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<ICreateOrderValidator, CreateOrderValidator>();
            services.AddTransient<IUpdateOrderValidator, UpdateOrderValidatior>();
            services.AddTransient<IOrderEventService, OrderEventService>();
            services.AddTransient<ICommunicationInfoService, CommunicationInfoService>();
            services.AddTransient<IPublisher, RabbitPublisher>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<ICommunicationInfoRepository, CommunicationInfoRepository>();
            services.AddScoped<ICommunicationInfoLogRepository, CommunicationInfoLogRepository>();
            
            services.AddDbContext<ApplicationDatabaseContext>(opt => opt.UseInMemoryDatabase(databaseName:"btcTurk"),ServiceLifetime.Scoped,ServiceLifetime.Scoped);

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Order.Api", Version = "v1" });
            });

            services.AddAutoMapper(typeof(Startup));
            services.AddHostedService<SmsListener>();
            services.AddHostedService<EmailListener>();
            services.AddHostedService<PushNotificationListener>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Order.Api v1"));
            }
            app.UseRouting();
            app.UseMiddleware<ExceptionHandlingMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
