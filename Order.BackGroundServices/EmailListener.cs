﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EasyNetQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Order.Data;
using Order.Domain;
using Order.Domain.Events;
using Order.Dto;
using Order.Service;

namespace Order.BackGroundServices
{
    public class EmailListener:IHostedService,IDisposable
    {
        private readonly ILogger<EmailListener> _logger;
        private readonly IConfiguration _configuration;
        private readonly IPublisher _publisher;
        public IServiceProvider Services { get; }
        private Timer _timer = null!;

        public EmailListener(IServiceProvider services,
            ILogger<EmailListener> logger, IOrderEventService orderEventService, IPublisher publisher, IConfiguration configuration)
        {
            _logger = logger;
            _publisher = publisher;
            _configuration = configuration;
            Services = services;
        }

       

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.FromSeconds(30), 
                Timeout.InfiniteTimeSpan);
            _logger.LogInformation("Event Lİstener Started");
            return Task.CompletedTask;


        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private void DoWork(object? state)
        {
            IAdvancedBus rabbitAdvancedBus = RabbitHutch
                .CreateBus(
                    _configuration.GetSection("RabbitConfig:ConnectionString").Value)
                .Advanced;
            var queue = rabbitAdvancedBus.QueueDeclare("Ord_Event_Email");


            rabbitAdvancedBus.Consume(new EasyNetQ.Topology.Queue("Ord_Event_Email", false),
                (message, properties, info) =>
                {

                    var data = Encoding.UTF8.GetString(message);
                    var communicationEvent = JsonConvert.DeserializeObject<CommunicationEvent>(data);
                    var response = SendEmail(communicationEvent);

                    communicationEvent.ResponseMessage = response.Message;
                    communicationEvent.RetryCount++;
                    communicationEvent.Success = response.Success;

                    if (!response.Success)
                    {

                        if (communicationEvent.RetryCount <= 3)
                        {
                            _publisher.Publish("Ord_Event_Email", communicationEvent);
                        }
                    }
                    SaveCommunicationInfo(communicationEvent);
                });
        }

        private BaseResponse SendEmail(CommunicationEvent communicationEvent)
        {
            var response = new BaseResponse()
            {
                Message = "",
                Success = true
            };
            return response;
        }

        private void SaveCommunicationInfo(CommunicationEvent communicationEvent)
        {
            using (var scope = Services.CreateScope())
            {
                var service = scope.ServiceProvider.GetService<ICommunicationInfoService>();
                service.UpdateCommunicationInfo(communicationEvent);
            }
        }



        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
