﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EasyNetQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Order.Domain;
using Order.Domain.Events;
using Order.Dto;
using Order.Service;

namespace Order.BackGroundServices
{
    public class PushNotificationListener : IHostedService,IDisposable
    {
        private readonly ILogger<PushNotificationListener> _logger;

        private readonly IPublisher _publisher;
        public IServiceProvider Services { get; }
        private IConfiguration _configuration;
        
        private Timer _timer = null!;

        public PushNotificationListener(IServiceProvider services,
            ILogger<PushNotificationListener> logger, IOrderEventService orderEventService,IPublisher publisher, IConfiguration configuration)
        {
            _logger = logger;
            _publisher = publisher;
            _configuration = configuration;
            Services = services;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Event Lİstener Started");
            _timer = new Timer(DoWork, null, TimeSpan.FromSeconds(30), 
                Timeout.InfiniteTimeSpan);
            return Task.CompletedTask;


        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private void DoWork(object? state)
        {
            IAdvancedBus rabbitAdvancedBus = RabbitHutch
                .CreateBus(
                    _configuration.GetSection("RabbitConfig:ConnectionString").Value)
                .Advanced;
            var queue = rabbitAdvancedBus.QueueDeclare("Ord_Event_PushNotification");
            rabbitAdvancedBus.Consume(new EasyNetQ.Topology.Queue("Ord_Event_PushNotification", false),
                (message, properties, info) =>
                {

                    var data = Encoding.UTF8.GetString(message);
                    var communicationEvent = JsonConvert.DeserializeObject<CommunicationEvent>(data);
                    var response = SendPushNotification(communicationEvent);
                    communicationEvent.ResponseMessage = response.Message;
                    communicationEvent.RetryCount++;
                    communicationEvent.Success = response.Success;


                    if (!response.Success)
                    {

                        if (communicationEvent.RetryCount <= 3)
                        {
                            _publisher.Publish("Ord_Event_Email", communicationEvent);
                        }
                    }
                    SaveCommunicationInfo(communicationEvent);
                });
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private BaseResponse SendPushNotification(CommunicationEvent communicationEvent)
        {
            var response = new BaseResponse()
            {
                Message = "",
                Success = false
            };
            return response;
        }

        private void SaveCommunicationInfo(CommunicationEvent communicationEvent)
        {
            using (var scope = Services.CreateScope())
            {
                var service = scope.ServiceProvider.GetService<ICommunicationInfoService>();
                service.UpdateCommunicationInfo(communicationEvent);
            }
        }
    }
}
