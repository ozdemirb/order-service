﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Crosscutting.Enums;

namespace Order.Crosscutting.Exceptions
{
    public abstract class BaseException : Exception
    {
        public BaseException(String type, string detail,ErrorType errorType) : this(type, detail,errorType, null, null)
        {
        }

        public BaseException(String type, string detail, ErrorType errorType,string entityName, string errorKey) : base(detail)
        {
            this.Type = type;
            this.Detail = detail;
            this.EntityName = entityName;
            this.ErrorKey = errorKey;
            this.ErrorType = errorType;
        }

        public String Type { get; set; }
        public string Detail { get; set; }
        public string EntityName { get; set; }
        public string ErrorKey { get; set; }

        public ErrorType ErrorType { get; set; }

    }
}
