﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Crosscutting.Enums;

namespace Order.Crosscutting.Exceptions
{
    public class BusinessValidationException : BaseException
    {
        public BusinessValidationException(string type, string detail,ErrorType errorType) : base(type, detail,errorType)
        {
        }

        public BusinessValidationException(string type, string detail,ErrorType errorType, string entityName, string errorKey) : base(type, detail,errorType, entityName, errorKey)
        {
        }
    }
    
}
