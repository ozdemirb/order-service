﻿using System;
using Order.Crosscutting.Enums;

namespace Order.Crosscutting.Exceptions
{
    public class DtoValidationException :BaseException
    {
        public DtoValidationException(string type, string detail) : base(type, detail, ErrorType.BadRequest)
        {
        }

        public DtoValidationException(string type, string detail,ErrorType errorType, string entityName, string errorKey) : base(type, detail,errorType, entityName, errorKey)
        {
        }
    }
}
