﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Order.Data.Mapping;

namespace Order.Data
{
    public class ApplicationDatabaseContext : DbContext
    {
        public ApplicationDatabaseContext(DbContextOptions<ApplicationDatabaseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            OrderMapping.OnModelCreating(builder);
            CommunicationInfoMapper.OnModelCreating(builder);
            CommunicationInfoLogMapper.OnModelCreating(builder);
            base.OnModelCreating(builder);
        }
    }
}
