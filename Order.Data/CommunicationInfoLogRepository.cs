﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Order.Domain;

namespace Order.Data
{
    public class CommunicationInfoLogRepository : RepositoryAggregate<CommunicationInfoLog>, ICommunicationInfoLogRepository
    {
        public CommunicationInfoLogRepository(ApplicationDatabaseContext context) : base(context)
        {
        }

        public CommunicationInfoLog GetById(Guid id)
        {
            var entity = context.Set<CommunicationInfoLog>()
                .FirstOrDefault(x => x.ID == id);
            return entity;
        }
    }
}
