﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Order.Domain;

namespace Order.Data
{
    public class CommunicationInfoRepository : RepositoryAggregate<CommunicationInfo>, ICommunicationInfoRepository
    {
        public CommunicationInfoRepository(ApplicationDatabaseContext context) : base(context)
        {
        }

        public CommunicationInfo GetById(Guid id)
        {
            var entity = context.Set<CommunicationInfo>()
                .FirstOrDefault(x => x.ID == id);
            return entity;
        }

        public CommunicationInfo GetByIdWitHLogs(Guid id)
        {
            var entity = context.Set<CommunicationInfo>().Include(x=>x.CommunicationInfoLogs)
                .FirstOrDefault(x => x.ID == id);
            return entity;
        }
    }
}
