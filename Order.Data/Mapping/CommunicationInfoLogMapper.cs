﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Order.Data.Mapping
{
    public static class CommunicationInfoLogMapper
    {
        public static void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Domain.CommunicationInfoLog>().ToTable("communicationInfoLog").HasKey(entity => entity.ID);

            modelBuilder.Entity<Domain.CommunicationInfoLog>().Property(x => x.CreateDate).HasColumnName("createDate");
            modelBuilder.Entity<Domain.CommunicationInfoLog>().Property(x => x.Content).HasColumnName("content");
            modelBuilder.Entity<Domain.CommunicationInfoLog>().Property(x => x.ResponseMessage).HasColumnName("responseMessage");
            modelBuilder.Entity<Domain.CommunicationInfoLog>().Property(x => x.Channel).HasColumnName("channel");
        }
    }
}
