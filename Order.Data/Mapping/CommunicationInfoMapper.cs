﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Order.Domain;

namespace Order.Data.Mapping
{
    public static class CommunicationInfoMapper
    {
        public static void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Domain.CommunicationInfo>().ToTable("communicationInfo").HasKey(entity => entity.ID);

            modelBuilder.Entity<Domain.CommunicationInfo>().Property(x => x.Channel).HasColumnName("communicationType");
            modelBuilder.Entity<Domain.CommunicationInfo>().Property(x => x.TargetAddress).HasColumnName("targetAddress");
            modelBuilder.Entity<Domain.CommunicationInfo>().Property(x => x.IsNotified).HasColumnName("isNotified");
            modelBuilder.Entity<Domain.CommunicationInfo>().HasMany<CommunicationInfoLog>(x => x.CommunicationInfoLogs)
                .WithOne(x => x.CommunicationInfo);
        }
    }
}
