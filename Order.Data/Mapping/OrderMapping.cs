﻿using Microsoft.EntityFrameworkCore;
using Order.Domain;

namespace Order.Data.Mapping
{
    public static class OrderMapping
    {
        public static void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Domain.Order>().ToTable("order").HasKey(entity => entity.ID);

            modelBuilder.Entity<Domain.Order>().Property(x => x.UserId).HasColumnName("userId");
            modelBuilder.Entity<Domain.Order>().Property(x => x.Amount).HasColumnName("amount");
            modelBuilder.Entity<Domain.Order>().Property(x => x.OrderDay).HasColumnName("orderDay");
            modelBuilder.Entity<Domain.Order>().Property(x => x.Status).HasColumnName("status");
            modelBuilder.Entity<Domain.Order>().HasMany<CommunicationInfo>(x => x.CommunicationInfos)
                .WithOne(x => x.Order);
        }
    }
}
