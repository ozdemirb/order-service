﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Order.Domain;

namespace Order.Data
{
    public class OrderRepository:RepositoryAggregate<Domain.Order>,IOrderRepository
    {
        public OrderRepository(ApplicationDatabaseContext context) : base(context)
        {
        }

        public  Domain.Order GetById(Guid id)
        {
            var entity = context.Set<Domain.Order>().Include(order => order.CommunicationInfos)
                .FirstOrDefault(x => x.ID == id);
            return entity;
        }
    }
}
