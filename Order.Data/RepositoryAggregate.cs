﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Order.Domain;

namespace Order.Data
{
    public class RepositoryAggregate<TEntity> : IRepository<TEntity> where TEntity:Entity{
        public readonly ApplicationDatabaseContext context;
        private DbSet<TEntity> entities;
        public RepositoryAggregate(ApplicationDatabaseContext context)
        {
            this.context = context;
            entities = context.Set<TEntity>();
        }

        public TEntity Insert(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            var response=entities.Add(entity);
            context.SaveChanges();
            return response.Entity;
        }

        public TEntity Update(TEntity entity)
        {
            context.Update(entity);
            context.SaveChanges();
            return entity;
        }

        public void Delete(TEntity entity)
        {
            context.Remove(entity);
            context.SaveChanges();
        }

        public List<TEntity> Find(Expression<Func<TEntity,bool>> predicate)
        {
            var response = context.Set<TEntity>().Where(predicate).ToList();
            return response;
        }
    }
}
