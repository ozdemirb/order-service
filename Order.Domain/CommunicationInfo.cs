﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Crosscutting.Enums;

namespace Order.Domain
{
    public class CommunicationInfo:Entity
    {
        public CommunicationInfo()
        {
            CommunicationInfoLogs = new List<CommunicationInfoLog>();
        }
        public Channel Channel { get; set; }

        public string TargetAddress { get; set; }

        public Order Order { get; set; }

        public bool IsNotified { get; set; }

        public List<CommunicationInfoLog> CommunicationInfoLogs { get; set; }
    }
}
