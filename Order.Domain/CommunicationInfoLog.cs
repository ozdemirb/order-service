﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Crosscutting.Enums;

namespace Order.Domain
{
    public class CommunicationInfoLog :Entity
    {
        public CommunicationInfo CommunicationInfo { get; set; }

        public DateTime CreateDate { get; set; }
        public string Content { get; set; }

        public string ResponseMessage { get; set; }
        
        public Channel Channel { get; set; }
        
        public bool Success { get; set; }
    }
}
