﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Crosscutting.Enums;

namespace Order.Domain.Events
{
    public class CommunicationEvent
    {
        public Guid CommunicationInfoId { get; set; }
        public string TargetAddress { get; set; }
        public int RetryCount { get; set; }
        public string Content { get; set; }
        public string ResponseMessage { get; set; }
        public bool Success { get; set; }
        public Channel Channel { get; set; }
    }
}
