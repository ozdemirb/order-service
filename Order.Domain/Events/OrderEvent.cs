﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Crosscutting.Enums;

namespace Order.Domain
{
    public class OrderEvent
    {
        public Guid OrderId { get; set; }

        public int RetryCount { get; set; }

        public int EventType { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public List<EventCommunication> CommunicationInfos{ get; set; }

        public string Content { get; set; }
    }

    public class  EventCommunication {

        public Guid CommunicationInfoId { get; set; }
        public Channel Channel { get; set; }

        public string TargetAddress { get; set; }

    }
}
