﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Order.Domain
{
    public interface ICommunicationInfoLogRepository : IRepository<CommunicationInfoLog>
    {
        CommunicationInfoLog GetById(Guid id);
    }

}
