﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Order.Domain
{
    public interface ICommunicationInfoRepository : IRepository<CommunicationInfo>
    {

        CommunicationInfo GetById(Guid id);

        CommunicationInfo GetByIdWitHLogs(Guid id);
    }
}
