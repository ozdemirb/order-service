﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Order.Domain
{
    public interface IEntity
    {
        public Guid ID { get; set; }
    }
}
