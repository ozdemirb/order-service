﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Order.Domain
{
    public interface IOrderRepository : IRepository<Order>
    {

        Order GetById(Guid id);
    }
}
