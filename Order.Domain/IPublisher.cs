﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Domain.Events;

namespace Order.Domain
{
    public interface IPublisher
    {
        void Publish(string queueName,CommunicationEvent communicationEvent);

    }
}
