﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Order.Domain
{
    public interface IRepository<TEntity>
    {
        TEntity Insert(TEntity entity);

        TEntity Update(TEntity entity);

        void Delete(TEntity entity);

        List<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
    }
}
