﻿using System;
using System.Collections.Generic;

namespace Order.Domain
{
    public class Order:Entity
    {
        
        public Guid UserId { get; set; }
        public string Amount { get; set; }

        public List<CommunicationInfo> CommunicationInfos { get; set; }

        public bool Status { get; set; }

        public int OrderDay { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }
    }
}
