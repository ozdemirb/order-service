﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Order.Dto
{
    public class CommunicationDto
    {
        public Guid OrderId { get; set; }

        public List<CommunicationInfoDto> CommunicationInfos { get; set; }
    }
}
