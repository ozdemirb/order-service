﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Crosscutting.Enums;

namespace Order.Dto
{
    public class CommunicationInfoLogDto
    {
        public Guid CommuncationInfoId { get; set; }

        public DateTime CreateDate { get; set; }
        public string Content { get; set; }

        public string ResponseMessage { get; set; }
        
        public bool Success { get; set; }
        
        public Channel Channel { get; set; }
    }
}
