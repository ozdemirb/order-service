// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this

using System;
using System.Collections.Generic;

namespace Order.Dto
{
    public class CommunicationLogDto
    {
        public List<CommunicationInfoLogDto> Logs { get; set; }
        
        public Guid OrderId { get; set; }
    }
}
