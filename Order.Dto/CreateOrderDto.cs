﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Order.Dto
{
    public class CreateOrderDto
    {
        public Guid UserId { get; set; }
        public string Amount { get; set; }

        public List<CommunicationInfoDto> CommunicationInfos { get; set; }

        public int OrderDay { get; set; }
    }
}
