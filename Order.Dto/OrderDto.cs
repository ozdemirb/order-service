﻿using Order.Crosscutting.Enums;
using System;
using System.Collections.Generic;

namespace Order.Dto
{
    public class OrderDto
    {
        public Guid OrderId { get; set; }
        public Guid UserId { get; set; }
        public string Amount { get; set; }

        public List<CommunicationInfoDto> CommunicationInfos { get; set; }

        public int OrderDay { get; set; }

        public bool Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

    }

    public class CommunicationInfoDto
    {
        public Channel Channel { get; set; }

        public string TargetAddress { get; set; }

        public bool IsNotified { get; set; }
    }

   
}
