﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Order.Dto
{
    public class OrderStatusOnlyDto
    {
        [JsonIgnore]
        public Guid OrderId { get; set; }
        public Guid UserId { get; set; }
    }
}
