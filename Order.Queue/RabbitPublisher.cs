﻿using System.Collections.Concurrent;
using System.Text;
using EasyNetQ;
using EasyNetQ.Topology;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Order.Domain;
using Order.Domain.Events;

namespace Order.Queue
{
    public class RabbitPublisher :IPublisher
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<RabbitPublisher> _logger;

        public RabbitPublisher(IConfiguration configuration, ILogger<RabbitPublisher> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public void Publish(string queueName, CommunicationEvent communicationEvent)
        {
            using (var rabbitAdvancedBus = RabbitHutch
                .CreateBus(
                    _configuration.GetSection("RabbitConfig:ConnectionString").Value)
                .Advanced)
            {
                var queue = rabbitAdvancedBus.QueueDeclare(queueName);
                var msg = new Message<CommunicationEvent>(communicationEvent);
                rabbitAdvancedBus.PublishAsync(Exchange.GetDefault(), queueName, false, msg);
            }
        }

    }
}
