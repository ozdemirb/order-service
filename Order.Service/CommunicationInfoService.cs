﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Order.Crosscutting.Enums;
using Order.Crosscutting.Exceptions;
using Order.Domain;
using Order.Domain.Events;
using Order.Dto;

namespace Order.Service
{
    public class CommunicationInfoService:ICommunicationInfoService
    {
        private readonly ICommunicationInfoRepository _communicationInfoRepository;
        private readonly ICommunicationInfoLogRepository _communicationInfoLogRepository;
        private readonly IMapper _mapper;
        private readonly IOrderRepository _orderRepository;

        public CommunicationInfoService(ICommunicationInfoRepository communicationInfoRepository, ICommunicationInfoLogRepository communicationInfoLogRepository, IMapper mapper, IOrderRepository orderRepository)
        {
            _communicationInfoRepository = communicationInfoRepository;
            _communicationInfoLogRepository = communicationInfoLogRepository;
            _mapper = mapper;
            _orderRepository = orderRepository;
        }

        public CommunicationDto GetCommunicationInfo(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);
            if (order == null || !order.Status)
            {
                throw new BusinessValidationException("business-validation", "Not Found", ErrorType.NotFound);
            }
            var response = new CommunicationDto()
            {
                OrderId = order.ID,
                CommunicationInfos = order.CommunicationInfos.Select(x => new CommunicationInfoDto()
                    { Channel = x.Channel, TargetAddress = x.TargetAddress,IsNotified = x.IsNotified}).ToList(),
            };

            return response;
        }

        public CommunicationLogDto GetOrderCommunicationInfoLog(Guid orderId)
        {
            var order = _orderRepository.GetById(orderId);
            if (order == null)
            {
                throw new BusinessValidationException("business-validation", "Not Found", ErrorType.NotFound);
            }
            var list = new List<CommunicationInfoLogDto>();
            foreach (var orderCommunicationInfo in order.CommunicationInfos)
            {
                var communicationInfoLogs =
                    _communicationInfoLogRepository.Find(x => x.CommunicationInfo.ID == orderCommunicationInfo.ID).ToList();
                foreach (var logs in communicationInfoLogs)
                {
                    var log = new CommunicationInfoLogDto()
                    {
                        CommuncationInfoId = orderCommunicationInfo.ID,
                        Channel = logs.Channel,
                        Content = logs.Content,
                        CreateDate = logs.CreateDate,
                        Success = logs.Success
                    };
                    list.Add(log);
                }
            }
            var response = new CommunicationLogDto() {Logs = list, OrderId = orderId};
            return response;
        }

        public void UpdateCommunicationInfo(CommunicationEvent communicationEvent)
        {
            var communicationInfo = _communicationInfoRepository.GetByIdWitHLogs(communicationEvent.CommunicationInfoId);
            if (communicationInfo == null)
            {
                throw new Exception("CommunicationInfo Not Found");
            }
            communicationInfo.CommunicationInfoLogs.Add(new CommunicationInfoLog()
            {
                Content = communicationEvent.Content,
                CreateDate = DateTime.Now,
                ResponseMessage = communicationEvent.ResponseMessage,
                Channel = communicationEvent.Channel,
                Success = communicationEvent.Success
            });
            if (communicationEvent.Success)
                communicationInfo.IsNotified = true;

            _communicationInfoRepository.Update(communicationInfo);
        }

    }
}
