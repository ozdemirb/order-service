﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Domain.Events;
using Order.Dto;

namespace Order.Service
{
    public interface ICommunicationInfoService
    {
        void UpdateCommunicationInfo(CommunicationEvent communicationEvent);
        CommunicationDto GetCommunicationInfo(Guid orderId);

        CommunicationLogDto GetOrderCommunicationInfoLog(Guid orderId);
    }
}
