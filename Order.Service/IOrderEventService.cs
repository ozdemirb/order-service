﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Order.Service
{
    public interface IOrderEventService
    {
        void PublishEvent(Domain.OrderEvent orderEvent);

        void TriggerOrderEvent(Domain.OrderEvent orderEvent);
    }
}
