﻿using System;
using Order.Dto;

namespace Order.Service
{
    public interface IOrderService
    {
        OrderDto CreateOrder(CreateOrderDto request);

        OrderDto GetOrder(Guid orderId);

        OrderDto UpdateOrderStatus(Guid orderId, OrderStatusOnlyDto request);
    }
}
