﻿using System.Threading.Tasks;
using Order.Crosscutting.Enums;
using Order.Domain;
using Order.Domain.Events;

namespace Order.Service
{
    public class OrderEventService : IOrderEventService
    {

        private readonly IPublisher _publisher;

        public OrderEventService(IPublisher publisher)
        {
            _publisher = publisher;
        }

        public void PublishEvent(OrderEvent order)
        {
            Parallel.ForEach(order.CommunicationInfos, info =>
            {
                var orderCommunicationEvent = new CommunicationEvent()
                {
                    TargetAddress = info.TargetAddress,
                    CommunicationInfoId = info.CommunicationInfoId,
                    Content = order.Content,
                    RetryCount = 0,
                    Channel = info.Channel
                };
                _publisher.Publish($"Ord_Event_{info.Channel.ToString()}", orderCommunicationEvent);
            });
            //foreach (var info in order.CommunicationInfos)
            //{

            //}
        }

       

        public void TriggerOrderEvent(OrderEvent orderEvent)
        {
            foreach (var communicationInfo in orderEvent.CommunicationInfos)
            {
                switch (communicationInfo.Channel)
                {
                    case Channel.Email:
                        SendEmail(communicationInfo.TargetAddress);
                        break;
                    case Channel.Sms:
                        SendEmail(communicationInfo.TargetAddress);
                        break;
                    case Channel.PushNotification:
                        SendEmail(communicationInfo.TargetAddress);
                        break;
                }
            }
        }

        private void SendEmail(string targetAddress)
        {

        }
    }
}
