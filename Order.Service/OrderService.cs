﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using Order.Crosscutting.Enums;
using Order.Crosscutting.Exceptions;
using Order.Domain;
using Order.Domain.Events;
using Order.Dto;
using Order.Service.Validation;

namespace Order.Service
{
    public class OrderService :IOrderService
    {
        private readonly ICreateOrderValidator _validator;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderEventService _orderEventService;
        private readonly ICommunicationInfoLogRepository _communicationInfoLogRepository;
        private readonly IMapper _mapper;

        public OrderService(ICreateOrderValidator validator, IOrderRepository orderRepository, IOrderEventService orderEventService, IMapper mapper, ICommunicationInfoLogRepository communicationInfoLogRepository)
        {
            _validator = validator;
            _orderRepository = orderRepository;
            _orderEventService = orderEventService;
            _mapper = mapper;
            _communicationInfoLogRepository = communicationInfoLogRepository;
        }

        public  OrderDto CreateOrder(CreateOrderDto request)
        {
            _validator.Valid(request);

            var userOrders=_orderRepository.Find(x => x.UserId == request.UserId && x.Status);
            if (userOrders.Any())
            {
                throw new BusinessValidationException("business-validation", "Already Active Order", ErrorType.BadRequest);
            }
            var order=_mapper.Map<Domain.Order>(request);
            order.Status = true;
            order=_orderRepository.Insert(order);
            var orderEvent = new OrderEvent()
            {
                OrderId = order.ID,
                CommunicationInfos = order.CommunicationInfos.Select(x=> new EventCommunication() { CommunicationInfoId =x.ID, Channel = x.Channel,TargetAddress = x.TargetAddress}).ToList(),
                CreateDate = DateTime.Now,
                Content = "Order Created successFully."
            };
            _orderEventService.PublishEvent(orderEvent);

            var response = _mapper.Map<OrderDto>(order);
            return response;
        }

        public OrderDto GetOrder(Guid orderId)
        {
            var order=_orderRepository.GetById(orderId);
            if (order == null)
            {
                throw new BusinessValidationException("business-validation", "Order Not Found", ErrorType.NotFound);
            }

            var response=_mapper.Map<OrderDto>(order);
            return response;
        }

        public OrderDto UpdateOrderStatus(Guid orderId, OrderStatusOnlyDto request)
        {
            
            var order = _orderRepository.GetById(orderId);
            if (order == null)
            {
                throw new BusinessValidationException("business-validation", "Not Found", ErrorType.NotFound);
            }

            if (!order.Status)
            {
                throw new BusinessValidationException("business-validation", "Not Found", ErrorType.NotFound);
            }

            if (order.UserId != request.UserId)
            {
                throw new BusinessValidationException("business-validation", "Not Found", ErrorType.NotFound);
            }
            order.Status = false;
            order.ModifyDate=DateTime.Now;
            _orderRepository.Update(order);
            var response = _mapper.Map<OrderDto>(order);
            return response;
        }
    }
}
