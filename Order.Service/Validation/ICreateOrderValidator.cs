﻿using System;
using System.Collections.Generic;
using System.Text;
using Order.Dto;

namespace Order.Service.Validation
{
    public interface ICreateOrderValidator
    {
        void Valid(CreateOrderDto order);
    }
}
