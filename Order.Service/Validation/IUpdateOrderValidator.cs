// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this

using System;
using Order.Dto;

namespace Order.Service.Validation
{
    public interface IUpdateOrderValidator
    {
        void Valid(OrderStatusOnlyDto order);
    }
}
