﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FluentValidation;
using Order.Crosscutting.Enums;
using Order.Crosscutting.Exceptions;
using Order.Dto;
using Order.Service.Validation;

namespace Order.Validation
{
    public class CreateOrderValidator : AbstractValidator<CreateOrderDto>, ICreateOrderValidator
    {

        public CreateOrderValidator()
        {
            RuleFor(x => x.UserId).NotEmpty().WithMessage("UserId is Empty").WithErrorCode("-100");
            RuleFor(x => x.UserId).NotEqual(Guid.Empty).WithMessage("UserId is Empty").WithErrorCode("-100");
            RuleFor(x => x.CommunicationInfos).NotNull().WithMessage("Communication Info not be empty")
                .WithErrorCode("-101");
            RuleFor(x => x.Amount).Must(IsValidAmount).When(x=>x.Amount!=null).NotNull().WithMessage("Amount Can Not be Empty").WithErrorCode("-102");
            RuleFor(x => x.OrderDay).Must(IsValidOrderDay).WithMessage("Order must be between 1-28")
                .WithErrorCode("-103");
            RuleFor(x => x.CommunicationInfos).Must(IsInEnum).WithMessage("Communication Type Not Valid")
                .WithErrorCode("-105");
        }
        public void Valid(CreateOrderDto order)
        {
            var result = this.Validate(order);
            if (!result.IsValid)
            {
                throw new DtoValidationException("validation-exception", result.Errors[0].ErrorMessage,ErrorType.BadRequest,
                    "",
                    result.Errors[0].PropertyName);
            }
        }

        public bool IsValidOrderDay(int orderDay)
        {
            if (orderDay > 28 || orderDay < 1)
            {
                return false;
            }

            return true;
        }

        public bool IsValidAmount(string requestAmount)
        {
            int amount = 0; 
            if (!int.TryParse(requestAmount, out amount))
            {
                return false;
            }

            if (amount < 100 || amount > 20000)
            {
                return false;
            }


            return true;
        }

        public bool IsInEnum(List<CommunicationInfoDto> communicationInfo)
        {
            if (communicationInfo.GroupBy(x => x.Channel).Count() != communicationInfo.Count)
            {
                return false;
            }

            foreach (var communication in communicationInfo)
            {
                if (!Enum.IsDefined(typeof(Channel),communication.Channel))
                {
                    return false;
                }

                if (String.IsNullOrEmpty(communication.TargetAddress))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
