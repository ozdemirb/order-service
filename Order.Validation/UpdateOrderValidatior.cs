// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this

using System;
using FluentValidation;
using Order.Crosscutting.Enums;
using Order.Crosscutting.Exceptions;
using Order.Dto;
using Order.Service.Validation;

namespace Order.Validation
{
    public class UpdateOrderValidatior: AbstractValidator<OrderStatusOnlyDto>, IUpdateOrderValidator
    {
        public UpdateOrderValidatior()
        {
            RuleFor(x => x.UserId).NotEmpty().WithMessage("UserId is Empty").WithErrorCode("-100");
            RuleFor(x => x.UserId).NotEqual(Guid.Empty).WithMessage("UserId is Empty").WithErrorCode("-100");
            RuleFor(x => x.OrderId).NotEqual(Guid.Empty).WithMessage("OrderId is Empty").WithErrorCode("-100");
        }

        public void Valid(OrderStatusOnlyDto order)
        {
            var result = this.Validate(order);
            if (!result.IsValid)
            {
                throw new DtoValidationException("validation-exception", result.Errors[0].ErrorMessage,ErrorType.BadRequest,
                    "",
                    result.Errors[0].PropertyName);
            }
        }
    }
}
