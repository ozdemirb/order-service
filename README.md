# Order Service

This application developer with .Net.Core 3.1 and Mysql 8.0.22

## Development

To start your application in the Debug configuration, simply run:

    docker-compose -f ./docker/rabbitmq.yml up
    dotnet run --verbosity normal --project ./Order.Api/Order.Api.csproj



## Building for production

To build the arifacts and optimize the orderService application for production, run:

    cd ./Order.Api
    rm -rf ./OrderApi/wwwroot
    dotnet publish --verbosity normal -c Release -o ./app/out ./OrderApi.csproj

The `./OrderApi.Api/app/out` directory will contain your application dll and its depedencies.

## Testing

To launch your application's tests, run:

    dotnet test --list-tests --verbosity normal

### Code quality

By Script :

1. Run Sonar in container : `docker-compose -f ./docker/sonar.yml up -d`

2. Wait container was up Run `SonarAnalysis.ps1` and go to http://localhost:9001


## Build a Docker image

You can also fully dockerize your application and all the services that it depends on. To achieve this, first build a docker image of your app by running:

    docker-compose -f orderService.yml up

